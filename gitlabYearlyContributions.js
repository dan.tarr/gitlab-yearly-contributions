console.log("GitLab contributions extension loaded!");

function randomString() {
	return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}

let pathname = window.location.pathname;
let validPath = false;
let username;
let elements = {
	totalContributions: {id: "", html: ""},
	additionalAnalytics: {id: "", html: ""}
};

if((pathname.match(/\//g) || []).length === 1 && pathname !== "/") { // if url matches /USERNAME
	username = pathname.substr(1);

	/*
		Check that the "username" in the pathname is actually a username
		if it exists on the page inside div.user-info then it's a user page
		otherwise its likely just /profile etc
	 */
	let containers = $("div.user-info")
	containers.each((index, element) => {
		if($(element).html().includes(username)) {
			validPath = true;
		}
	});
} else if((pathname.match(/\//g) || []).length === 2 && pathname.endsWith("/")) { // if pathname matches /USERNAME/
	username = pathname.substr(1, pathname.length - 1);
	validPath = true;
}

if(validPath) {
	$.ajax({
		url: `/users/${username}/calendar.json`,
		method: "GET",
		success: function (result) {
			let dates = Object.keys(result);
			let total = 0;
			let lowest = Number.POSITIVE_INFINITY;
			let highest = Number.NEGATIVE_INFINITY;

			// count contributions
			dates.forEach(key => {
				let value = result[key];

				total += value;

				if (value < lowest)
					lowest = value;

				if (value > highest)
					highest = value;
			});

			let average = Math.round(total / dates.length);

			let totalContributionsId = randomString();
			let additionalAnalyticsId = randomString();

			elements.totalContributions = {
				id: totalContributionsId,
				html: `<h4 id="${totalContributionsId}" class="yearly-contribs">${total} contributions in the last year</h4>`
			};
			elements.additionalAnalytics = {
				id: additionalAnalyticsId,
				html: `<div id="${additionalAnalyticsId}">
					<h4 class="yearly-contribs mb-1 mt-0">Average per day: ${average}</h4>
					<h4 class="yearly-contribs mb-1 mt-0">Lowest contributions: ${lowest}</h4>
					<h4 class="yearly-contribs mb-1 mt-0">Highest contributions: ${highest}</h4>
				</div>`
			};

			let calendarContainer = $(".calendar");

			calendarContainer.before(elements.totalContributions.html);
			calendarContainer.after(elements.additionalAnalytics.html);
		}
	});

	// When the window resizes, for some reason GitLab removes the added elements
	// Keep checking if they exist, if not, replace them
	setInterval(() => {
		if (!elements.additionalAnalytics.id || !elements.totalContributions.id)
			return;

		let totalContributionsElement = $(`#${elements.totalContributions.id}`);
		let additionalAnalyticsElement = $(`#${elements.additionalAnalytics.id}`);

		if (totalContributionsElement.length === 0) {
			let calendarContainer = $(".calendar");
			calendarContainer.before(elements.totalContributions.html);
		}

		if (additionalAnalyticsElement.length === 0) {
			let calendarContainer = $(".calendar");
			calendarContainer.after(elements.additionalAnalytics.html);
		}
	}, 500);
}
